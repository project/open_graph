<?php


/**
 * Settings form
 */
function open_graph_settings_form(){
  $form = array();

  // @todo:
  // - Global image
  // - 
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['open_graph_general_type'] = array(
      '#type' => 'select',
      '#title' => t('Default Open Graph protocol type'),
      '#description' => t('Choose the default Open Graph protocol type you want to associate to pages.'),
      '#default_value' => variable_get('open_graph_general_type', ''),
      '#options' => get_open_graph_types(),
  );
    
  $form = system_settings_form($form);
  return $form;
}